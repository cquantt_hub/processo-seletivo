# Desafio Full-Stack Júnior

O objetivo geral do desafio é validar os conhecimentos básicos de Programação Web.

## O que será avaliado?
- A qualidade do seu código.
- As decisões que você fez para resolver o desafio.
- Como você utiliza suas ferramentas.

## Problema

Precisamos criar um catálogo online de filmes. Nesse catálogo web deve ser possível visualizar as informações básicas dos filmes em uma listagem e realizar buscas para filtrar a informação.

O catálogo deve ser o mais organizado possível para facilitar a visualização e busca pelo usuário.

### Atividades

* Criar uma página web capaz de listar os filmes da base de dados
* Criar a funcionalidade de busca para filtrar a informação listada
* Publicar o código no [GitHub](http://github.com)
* Publicar o sistema no [Heroku](https://www.heroku.com/)

## Dados

O Kaggle disponibiliza uma base de dados que pode ser utilizada na solução do problema:

* https://www.kaggle.com/neha1703/movie-genre-from-its-poster

Observação: Caso baixe o dataset pelo Kaggle, será necessário atualizar os links dos filmes adicionando um dígito zero após '/title/tt', ou seja, para o link:
http://www.imdb.com/title/tt114709
após a atualização deverá ficar:
http://www.imdb.com/title/tt0114709

Pode-se baixar o dataset também esse mesmo dataset já atualizado em: https://gitlab.com/cquantt_hub/processo-seletivo no diretório 'Dataset'.

## FAQ

#### Posso usar qualquer linguagem/framework?

Sim. Desde que seja web pode usar qualquer linguagem/framework para realizar o desafio.

#### Posso utilizar um template pronto e adaptar para a solução?

Sim.

#### Posso utilizar mais de uma base de dados para o problema?

Sim. A base apresentada é uma sugestão, caso julgue necessário pode utilizar qualquer outra base de dados que esteja disponível na internet.


Fique à vontade caso queira implementar funcionalidades adicionais, ou até mesmo implementar testes unitários.
Boa sorte e bom desafio!
